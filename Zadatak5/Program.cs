﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            List<IRentable> rentables = new List<IRentable>();
            Book book = new Book("Vlak u snijegu");
            Video video = new Video("Harry Potter i Kamen Mudraca");
            Book book1 = new Book("Druzba Pere Kvrzice");
            Video video1 = new Video("Die Hard");
            HotItem hotbook = new HotItem(book1);
            HotItem hotvideo = new HotItem(video1);
            rentables.Add(book);
            rentables.Add(video);
            rentables.Add(hotbook);
            rentables.Add(hotvideo);
            rentingConsolePrinter.DisplayItems(rentables);
            rentingConsolePrinter.PrintTotalPrice(rentables);

            List<IRentable> rentables2 = new List<IRentable>();
            rentables2.Add(new DiscountedItem(book, 25));
            rentables2.Add(new DiscountedItem(video, 40));
            rentables2.Add(new DiscountedItem(hotbook, 50));
            rentables2.Add(new DiscountedItem(hotvideo, 50));
            rentingConsolePrinter.DisplayItems(rentables2);
            rentingConsolePrinter.PrintTotalPrice(rentables2);
        }
    }
}
