﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class DiscountedItem : RentableDecorator
    {
        private double DiscountPercentage = 25;
        public DiscountedItem(IRentable rentable, double discountPercentage) : base(rentable) 
        { 
            DiscountPercentage = discountPercentage; 
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() * (this.DiscountPercentage/100);
        }
        public override String Description
        {
            get
            {
                return base.Description + " now at " + DiscountPercentage +"% off!";
            }
        }
    }
}
