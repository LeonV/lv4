﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            List<IRentable> rentables = new List<IRentable>();
            Book book = new Book("Vlak u snijegu");
            Video video = new Video("Harry Potter i Kamen Mudraca");
            rentables.Add(book);
            rentables.Add(video);
            Book book1 = new Book("Druzba Pere Kvrzice");
            Video video1 = new Video("Die Hard");
            HotItem hotbook = new HotItem(book1);
            HotItem hotvideo = new HotItem(video1);
            rentables.Add(hotbook);
            rentables.Add(hotvideo);
            rentingConsolePrinter.DisplayItems(rentables);
            rentingConsolePrinter.PrintTotalPrice(rentables);
            //Nakon ispisa vidimo da wrapper mjenja description hit knjige i hit videa bez da mjenja osnovni objekt
        }
    }
}
