﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class EmailValidator : IEmailValidatorService
    {
        public int MinLength { get; private set; }
        public EmailValidator(int minLength)
        {
            this.MinLength = minLength;
        }
        public bool IsValidAddress(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return IsLongEnough(candidate) && ContainsAtSymbol(candidate) && EndsWithDomain(candidate);
        }
        private bool IsLongEnough(String candidate)
        {
            return candidate.Length >= this.MinLength;
        }
        private bool ContainsAtSymbol(String candidate)
        {
            return candidate.Contains("@");
        }
        private bool EndsWithDomain(String candidate)
        {
            return (candidate.EndsWith(".com") || candidate.EndsWith(".hr"));
        }
    }
}
