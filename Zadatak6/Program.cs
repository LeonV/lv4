﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            EmailValidator emailValidator = new EmailValidator(6);
            string mail1 = "bananko@mail.com";
            string mail2 = "@.hr";
            string mail3 = "bananko@mail.dsadasd";
            Console.WriteLine(mail1 + " Is correct: " + emailValidator.IsValidAddress(mail1));
            Console.WriteLine(mail2 + " Is correct: " + emailValidator.IsValidAddress(mail2));
            Console.WriteLine(mail3 + " Is correct: " + emailValidator.IsValidAddress(mail3));
        }
    }
}
