﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            List<IRentable> rentables = new List<IRentable>();
            Book book = new Book("Vlak u snijegu");
            Video video = new Video("Harry Potter i Kamen Mudraca");
            rentables.Add(book);
            rentables.Add(video);
            rentingConsolePrinter.DisplayItems(rentables);
            rentingConsolePrinter.PrintTotalPrice(rentables);
        }
    }
}
