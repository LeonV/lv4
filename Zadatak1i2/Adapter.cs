﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1i2
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            int i = 0;
            int j = 0;
            int rowCount = 0;
            int collumnCount = 0;
            foreach (List<double> Rows in dataset.GetData())
            {
                rowCount++;
            }
            double[][] temp = new double[rowCount][];
            foreach (List<double> Rows in dataset.GetData())
            {
                foreach (double Element in Rows)
                {
                    collumnCount++;
                }
                temp[i] = new double[collumnCount];
                foreach (double Element in Rows)
                {
                    temp[i][j] = Element;
                    j++;
                }
                collumnCount = 0;
                j = 0;
                i++;
            }
            return temp;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}