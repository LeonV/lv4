﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1i2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Napomena: Regionalni Format Windowsa je na English(United States)
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\data.csv";
            Dataset dataSet = new Dataset(path);
            Analyzer3rdParty service = new Analyzer3rdParty();
            Adapter adapter = new Adapter(service);

            double[] columnAverage = adapter.CalculateAveragePerColumn(dataSet);
            double[] rowAverage = adapter.CalculateAveragePerRow(dataSet);

            Console.WriteLine("Average Per Row:");
            foreach (double number in rowAverage)
            {
                Console.WriteLine(number.ToString());
            }
            Console.WriteLine("Average Per Column:");
            
            foreach (double number in columnAverage)
            {
                Console.WriteLine(number.ToString());
            }

        }
    }
}
