﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1i2
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int columnCount = 0;
            foreach (double[] Row in data)
            {
                foreach (double Element in Row)
                {
                    columnCount++;
                }
            }
            columnCount = columnCount/data.Length;
            double[] results = new double[columnCount];
            foreach (double[] Row in data)
            {
                int i = 0;
                foreach (double Element in Row)
                {
                    results[i] = results[i] + Element;
                    i++;
                }
            }
            for (int i = 0; i < columnCount; i++)
                results[i] = results[i] / data.Length;
            return results;
        }
    }
}
