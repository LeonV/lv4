﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class RegistrationValidator : IRegistrationValidator
    {

        private EmailValidator emailValidator;
        private PasswordValidator passwordValidator;
        public RegistrationValidator(){
            emailValidator = new EmailValidator(6);
            passwordValidator = new PasswordValidator(6);
        }
        public RegistrationValidator(int minEmailLenght, int minPasswordLenght){
            emailValidator = new EmailValidator(minEmailLenght);
            passwordValidator = new PasswordValidator(minPasswordLenght);
        }
        public bool IsUserEntryValid(UserEntry entry) {

            return passwordValidator.IsValidPassword(entry.Password) && emailValidator.IsValidAddress(entry.Email);
        }
    }
}
