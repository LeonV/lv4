﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            RegistrationValidator registrationValidator = new RegistrationValidator(6, 8);
            bool check;
            do
                check = registrationValidator.IsUserEntryValid(UserEntry.ReadUserFromConsole());
            while (check == false);
            Console.WriteLine("Sucessful Registration!");

        }
    }
}
